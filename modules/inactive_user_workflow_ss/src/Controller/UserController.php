<?php

namespace Drupal\inactive_user_workflow_ss\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\inactive_user_workflow\InactiveUser;
use Drupal\inactive_user_workflow\InactiveUserInterface;
use Drupal\inactive_user_workflow_ss\Form\UserAccountRecoveryForm;
use Drupal\inactive_user_workflow_ss\InactiveUsersSelfService;
use Drupal\user\Controller\UserController as CoreUserController;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for self-service account reset.
 */
class UserController extends CoreUserController {

  /**
   * The inactive user workflow self-service service.
   *
   * @var \Drupal\inactive_user_workflow_ss\InactiveUsersSelfService
   */
  protected InactiveUsersSelfService $inactiveUsersSS;

  /**
   * Constructs a UserController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\inactive_user_workflow_ss\InactiveUsersSelfService $inactive_user_workflow_ss
   *   The inactive user workflow self-service service.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    UserStorageInterface $user_storage,
    UserDataInterface $user_data,
    LoggerInterface $logger,
    FloodInterface $flood,
    InactiveUsersSelfService $inactive_user_workflow_ss
  ) {
    parent::__construct($date_formatter, $user_storage, $user_data, $logger, $flood);
    $this->inactiveUsersSS = $inactive_user_workflow_ss;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('user.data'),
      $container->get('logger.factory')->get('user'),
      $container->get('flood'),
      $container->get('inactive_user_workflow_ss.service')
    );
  }

  /**
   * Redirects to the account recovery reset form.
   *
   * In order to never disclose a reset link via a referrer header this
   * controller must always return a redirect response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param int $uid
   *   User ID of the user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function recoverAccount(Request $request, $uid, $timestamp, $hash) {
    $account = $this->currentUser();
    // When processing the one-time login link, we have to make sure that a user
    // isn't already logged in.
    if ($account->isAuthenticated()) {
      // The current user is already logged in.
      if ($account->id() == $uid) {
        user_logout();
        // We need to begin the redirect process again because logging out will
        // destroy the session.
        return $this->redirect(
          'user.reset',
          [
            'uid' => $uid,
            'timestamp' => $timestamp,
            'hash' => $hash,
          ]
        );
      }
      // A different user is already logged in on the computer.
      else {
        /** @var \Drupal\user\UserInterface $reset_link_user */
        if ($reset_link_user = $this->userStorage->load($uid)) {
          $this->messenger()
            ->addWarning($this->t('Another user (%other_user) is already logged into the site on this computer, but you tried to use an account recovery link for user %resetting_user. Please <a href=":logout">log out</a> and try using the link again.',
              [
                '%other_user' => $account->getAccountName(),
                '%resetting_user' => $reset_link_user->getAccountName(),
                ':logout' => Url::fromRoute('user.logout')->toString(),
              ]));
        }
        else {
          // Invalid one-time link specifies an unknown user.
          $this->messenger()->addError($this->t('The one-time login link you clicked is invalid.'));
        }
        return $this->redirect('<front>');
      }
    }

    /** @var \Drupal\user\UserInterface $reset_link_user */
    $reset_link_user = $this->userStorage->load($uid);
    if ($redirect = $this->determineErrorRedirect($reset_link_user, $timestamp, $hash)) {
      return $redirect;
    }

    $session = $request->getSession();
    $session->set('pass_reset_hash', $hash);
    $session->set('pass_reset_timeout', $timestamp);
    return $this->redirect(
      'inactive_user_workflow_ss.recovery.form',
      ['uid' => $uid]
    );
  }

  /**
   * Validates user, hash, and timestamp.
   *
   * This method allows the 'inactive_user_workflow_ss.recovery' and
   * 'inactive_user_workflow_ss.recovery.login' routes to use
   * the same logic to check the user, timestamp and hash and redirect to the
   * same location with the same messages.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   User requesting reset. NULL if the user does not exist.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Returns a redirect if the information is incorrect. It redirects to
   *   'inactive_user_workflow_ss.reset.step_1' route with a message
   *   for the user.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If $uid is for an active user or invalid user ID.
   */
  protected function determineErrorRedirect(?UserInterface $user, int $timestamp, string $hash): ?RedirectResponse {
    $current = REQUEST_TIME;
    // Verify that the user exists and is active.
    if (
      $user === NULL ||
      !UserController::userIsBlockedBecauseOfInactivity('', (int) $user->id())
    ) {
      // Active or invalid user ID, so deny access. The parameters will be in
      // the watchdog's URL for the administrator to check.
      throw new AccessDeniedHttpException();
    }

    // Time out, in seconds, until login URL expires.
    $timeout = $this->config('user.settings')->get('password_reset_timeout');
    // No time out for first time login.
    if ($user->getLastLoginTime() && $current - $timestamp > $timeout) {
      $this->messenger()->addError($this->t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.'));
      return $this->redirect('inactive_user_workflow_ss.reset.step_1');
    }
    elseif ($user->isAuthenticated() && ($timestamp >= $user->getLastLoginTime()) && ($timestamp <= $current) && hash_equals($hash, $this->inactiveUsersSS->userRecoveryHash($user, $timestamp))) {
      // The information provided is valid.
      return NULL;
    }

    $this->messenger()->addError($this->t('You have tried to use a one-time link that has either been used or is no longer valid. Please request a new one using the form below.'));
    return $this->redirect('inactive_user_workflow_ss.reset.step_1');
  }

  /**
   * Returns the account recovery form.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param int $uid
   *   User ID of the user requesting reset.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The form structure or a redirect response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the pass_reset_timeout or pass_reset_hash are not available in the
   *   session. Or if $uid is for an active user or invalid user ID.
   */
  public function getAccountRecoveryForm(Request $request, $uid) {
    $session = $request->getSession();
    $timestamp = $session->get('pass_reset_timeout');
    $hash = $session->get('pass_reset_hash');
    // As soon as the session variables are used they are removed to prevent the
    // hash and timestamp from being leaked unexpectedly. This could occur if
    // the user does not click on the log in button on the form.
    $session->remove('pass_reset_timeout');
    $session->remove('pass_reset_hash');
    if (!$hash || !$timestamp) {
      throw new AccessDeniedHttpException();
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->userStorage->load($uid);
    if (
      $user === NULL ||
      !UserController::userIsBlockedBecauseOfInactivity('', (int) $user->id())
    ) {
      // Active or invalid user ID, so deny access. The parameters will be in
      // the watchdog's URL for the administrator to check.
      throw new AccessDeniedHttpException();
    }

    // Time out, in seconds, until login URL expires.
    $timeout = $this->config('user.settings')->get('password_reset_timeout');

    $expiration_date = $user->getLastLoginTime() ? $this->dateFormatter->format($timestamp + $timeout) : NULL;
    return $this->formBuilder()->getForm(UserAccountRecoveryForm::class, $user, $expiration_date, $timestamp, $hash);
  }

  /**
   * Validates user, hash, and timestamp; logs the user in if correct.
   *
   * @param int $uid
   *   User ID of the user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the user edit form if the information is correct.
   *   If the information is incorrect redirects to 'user.pass' route with a
   *   message for the user.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If $uid is for a blocked user or invalid user ID.
   */
  public function resetPassLogin($uid, $timestamp, $hash, Request $request) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->userStorage->load($uid);
    if ($redirect = $this->determineErrorRedirect($user, $timestamp, $hash)) {
      return $redirect;
    }

    $flood_config = $this->config('user.flood');
    if ($flood_config->get('uid_only')) {
      $identifier = $user->id();
    }
    else {
      $identifier = $user->id() . '-' . $request->getClientIP();
    }

    $this->flood->clear('user.failed_login_user', $identifier);
    $this->flood->clear('user.http_login', $identifier);

    // Reactivate the user.
    $user
      ->activate()
      ->save();

    user_login_finalize($user);
    $this->logger->info('User %name used one-time login link at time %timestamp.', [
      '%name' => $user->getDisplayName(),
      '%timestamp' => $timestamp,
    ]);
    $this->messenger()->addStatus($this->t('You have just used your one-time login link. It is no longer necessary to use this link to log in. Please set your password.'));
    // Let the user's password be changed without the current password
    // check.
    $token = Crypt::randomBytesBase64(55);
    $request->getSession()->set('pass_reset_' . $user->id(), $token);
    // Clear any flood events for this user.
    $this->flood->clear('user.password_request_user', $uid);
    return $this->redirect(
      'entity.user.edit_form',
      ['user' => $user->id()],
      [
        'query' => ['pass-reset-token' => $token],
        'absolute' => TRUE,
      ]
    );
  }

  /**
   * Sets an error if supplied username has been blocked and link to recovery.
   */
  public static function validateName(array &$form, FormStateInterface $form_state) {
    if (
      !$form_state->isValueEmpty('name') &&
      static::userIsBlockedBecauseOfInactivity($form_state->getValue('name'))
    ) {
      // Blocked in user administration.
      $form_state->setErrorByName(
        'name',
        t('The username %name has not been activated or is blocked by inactivity. You may recover your account here: <a href="@recovery">Account Recovery</a>', [
          '%name' => $form_state->getValue('name'),
          '@recovery' => Url::fromRoute('inactive_user_workflow_ss.reset.step_1')->toString(),
        ])
      );
    }
  }

  /**
   * Checks for usernames currently blocked by inactivity.
   *
   * @param string $name
   *   A string containing a name of the user.
   * @param int|null $id
   *   The user ID.
   *
   * @return bool
   *   TRUE if the user is blocked, FALSE otherwise.
   */
  public static function userIsBlockedBecauseOfInactivity(string $name = '', int $id = NULL): bool {
    if (empty($name) && empty($id)) {
      return FALSE;
    }

    $storage = \Drupal::entityTypeManager()->getStorage('user');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 0)
      ->range(0, 1);
    if ($id) {
      $query->condition('uid', $id);
    }
    else {
      $query->condition('name', $name);
    }
    $ids = $query->execute();

    if ($ids) {
      $workflow = InactiveUser::getActiveWorkflowForUser((int) reset($ids));
      return $workflow->getCurrentStatus() == InactiveUserInterface::STATUS_ACCOUNT_BLOCKED;
    }

    return FALSE;
  }

}
