<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow_ss;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;

/**
 * Service that manages inactive user workflows self-service tasks.
 */
class InactiveUsersSelfService {

  /**
   * The configuration for the parent module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $configInactiveUserWorkflow;

  /**
   * The configuration for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected readonly EntityStorageInterface $userStorage;

  /**
   * The time this job started executing.
   *
   * @var int
   */
  protected int $timestamp;

  /**
   * Constructs an InactiveUsersWorkflowService object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly Connection $connection,
    private readonly MailManagerInterface $pluginManagerMail,
    private readonly LoggerInterface $logger,
  ) {
    $this->configInactiveUserWorkflow = $configFactory->get('inactive_user_workflow.settings');
    $this->config = $configFactory->get('inactive_user_workflow_ss.settings');
    $this->userStorage = $this->entityTypeManager->getStorage('user');
  }

  /**
   * Mail one time login URL and instructions.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   */
  public function sendAccountRecoveryEmail(UserInterface $account): void {
    $this->sendNotification(
      $this->config->get('template_subject_account_recovery'),
      $this->config->get('template_body_account_recovery'),
      $account->getEmail(),
      $account
    );
    $this->logger
      ->info('Account recovery instructions mailed to %name at %email.', [
        '%name' => $account->getAccountName(),
        '%email' => $account->getEmail(),
      ]);
  }

  /**
   * Send the email notification.
   *
   * @param string $subject
   *   The subject string.
   * @param string $body
   *   The body of the mail.
   * @param string $to
   *   The email.
   * @param \Drupal\user\UserInterface $account
   *   The user.
   *
   * @see inactive_user_workflow_mail()
   */
  protected function sendNotification(string $subject, string $body, string $to, UserInterface $account) {
    $params['subject'] = $subject;
    $params['body'] = $body;
    $params['to'] = $to;
    $params['reply-to'] = $this->configInactiveUserWorkflow->get('contact_email');
    $params['account'] = $account;
    $params['language'] = $account->language()->getId();

    $this->pluginManagerMail->mail('inactive_user_workflow', 'account_recovery', $to, $account->language()->getId(), $params);
  }

  /**
   * Generates a unique URL for a user to recover their account.
   *
   * @param \Drupal\user\UserInterface $account
   *   An object containing the user account.
   * @param array $options
   *   (optional) A keyed array of settings. Supported options are:
   *   - langcode: A language code to be used when generating locale-sensitive
   *    URLs. If langcode is NULL the users preferred language is used.
   *
   * @return string
   *   A unique URL that provides a one-time link for the user.
   */
  public function userRecoveryUrl($account, $options = []): string {
    if ($account->isActive()) {
      return Url::fromRoute('user.page', [], ['absolute' => TRUE])->toString();
    }

    $timestamp = \Drupal::time()->getRequestTime();
    $langcode = $options['langcode'] ?? $account->getPreferredLangcode();

    return Url::fromRoute('inactive_user_workflow_ss.recovery',
      [
        'uid' => $account->id(),
        'timestamp' => $timestamp,
        'hash' => $this->userRecoveryHash($account, $timestamp),
      ],
      [
        'absolute' => TRUE,
        'language' => \Drupal::languageManager()->getLanguage($langcode),
      ]
    )->toString();
  }


  /**
   * Creates a unique hash value for use in time-dependent per-user URLs.
   *
   * This hash is normally used to build a unique and secure URL that is sent to
   * the user by email for purposes such as resetting the user's password. In
   * order to validate the URL, the same hash can be generated again, from the
   * same information, and compared to the hash value from the URL. The hash
   * contains the time stamp, the user's last login time, the numeric user ID,
   * and the user's email address.
   * For a usage example, see user_cancel_url() and
   * \Drupal\user\Controller\UserController::confirmCancel().
   *
   * @param \Drupal\user\UserInterface $account
   *   An object containing the user account.
   * @param int $timestamp
   *   A UNIX timestamp, typically \Drupal::time()->getRequestTime().
   *
   * @return string
   *   A string that is safe for use in URLs and SQL statements.
   */
  public function userRecoveryHash(UserInterface $account, $timestamp): string {
    $data = $timestamp;
    $data .= $account->getLastLoginTime();
    $data .= $account->id();
    $data .= $account->getEmail();
    $data .= (int) $account->isActive();
    return Crypt::hmacBase64($data, Settings::getHashSalt() . $account->getPassword());
  }

}
