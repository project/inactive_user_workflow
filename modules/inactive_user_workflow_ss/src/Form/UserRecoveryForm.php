<?php

namespace Drupal\inactive_user_workflow_ss\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\inactive_user_workflow_ss\Controller\UserController;
use Drupal\inactive_user_workflow_ss\InactiveUsersSelfService;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a user account recovery form.
 *
 * Send the user an email to recover their account.
 */
class UserRecoveryForm extends FormBase {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected UserStorageInterface $userStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected FloodInterface $flood;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected TypedDataManagerInterface $typedDataManager;

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected EmailValidatorInterface $emailValidator;

  /**
   * The inactive user workflow self-service service.
   *
   * @var \Drupal\inactive_user_workflow_ss\InactiveUsersSelfService
   */
  protected InactiveUsersSelfService $inactiveUsersSelfService;

  /**
   * Constructor.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\inactive_user_workflow_ss\InactiveUsersSelfService $inactive_users_self_service
   *   The inactive user workflow self-service service.
   */
  public function __construct(
    UserStorageInterface $user_storage,
    LanguageManagerInterface $language_manager,
    ConfigFactory $config_factory,
    FloodInterface $flood,
    TypedDataManagerInterface $typed_data_manager,
    EmailValidatorInterface $email_validator,
    InactiveUsersSelfService $inactive_users_self_service
  ) {
    $this->userStorage = $user_storage;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->flood = $flood;
    $this->typedDataManager = $typed_data_manager;
    $this->emailValidator = $email_validator;
    $this->inactiveUsersSelfService = $inactive_users_self_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('flood'),
      $container->get('typed_data_manager'),
      $container->get('email.validator'),
      $container->get('inactive_user_workflow_ss.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_account_recovery_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address'),
      '#size' => 60,
      '#maxlength' => Email::EMAIL_MAX_LENGTH,
      '#required' => TRUE,
      '#attributes' => [
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ],
    ];

    $form['mail'] = [
      '#prefix' => '<p>',
      '#markup' => $this->t('Account reset instructions will be sent to your registered email address.'),
      '#suffix' => '</p>',
    ];

    $form['name']['#default_value'] = $this->getRequest()->query->get('name');

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#cache']['contexts'][] = 'url.query_args';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $flood_config = $this->configFactory->get('user.flood');
    if (!$this->flood->isAllowed('user.account_reset_request_ip', $flood_config->get('ip_limit'), $flood_config->get('ip_window'))) {
      $form_state->setErrorByName('name', $this->t('Too many account recovery requests from your IP address. It is temporarily blocked. Try again later or contact the site administrator.'));
      return;
    }
    $this->flood->register('user.account_reset_request_ip', $flood_config->get('ip_window'));

    $mail = trim($form_state->getValue('name'));


    // Try to load by email.
    $users = $this->userStorage->loadByProperties(['mail' => $mail]);
    $account = reset($users);

    // Non-blocked accounts cannot request a recovery.
    if (
      $account &&
      $account->id() &&
      UserController::userIsBlockedBecauseOfInactivity('', (int) $account->id())
    ) {
      // Register flood events based on the uid only, so they apply for any
      // IP address. This allows them to be cleared on successful reset (from
      // any IP).
      $identifier = $account->id();
      if (!$this->flood->isAllowed('user.account_reset_request_user', $flood_config->get('user_limit'), $flood_config->get('user_window'), $identifier)) {
        return;
      }
      $this->flood->register('user.account_reset_request_user', $flood_config->get('user_window'), $identifier);
      $form_state->setValueForElement(['#parents' => ['account']], $account);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = $form_state->getValue('account');
    if ($account) {
      // Mail one time login URL and instructions, and log this action.
      $this->inactiveUsersSelfService->sendAccountRecoveryEmail($account);
    }
    else {
      $this->logger('inactive_user_workflow_ss')
        ->info('Account recovery form was submitted with an unknown or inactive account: %name.', [
          '%name' => $form_state->getValue('name'),
        ]);
    }
    // Make sure the status text is displayed even if no email was sent. This
    // message is deliberately the same as the success message for privacy.
    $this->messenger()
      ->addStatus($this->t('If %identifier is a valid account, an email will be sent with instructions to recover your account.', [
        '%identifier' => $form_state->getValue('name'),
      ]));

    $form_state->setRedirect('<front>');
  }

}
