<?php

/**
 * @file
 * Provide views data for the inactive_user_workflow module.
 */

/**
 * Implements hook_views_data().
 */
function inactive_user_workflow_views_data() {
  $data['inactive_user_workflow']['table']['group'] = t('Inactive user workflow');
  $data['inactive_user_workflow']['table']['base'] = [
    'field' => 'id',
    'title' => t('Inactive user workflows'),
    'help' => t('Keep track of inactive user workflows.'),
  ];

  // Fields.
  $data['inactive_user_workflow']['id'] = [
    'title' => t('ID'),
    'help' => t('Unique ID for the workflow.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['inactive_user_workflow']['uid'] = [
    'title' => t('User'),
    'help' => t('The associated user.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'relationship' => [
      'id' => 'standard',
      'base' => 'users_field_data',
      'base field' => 'uid',
    ],
  ];

  $data['inactive_user_workflow']['first_notice_ts'] = [
    'title' => t('First notice'),
    'help' => t('The date and time when first notice was sent.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  $data['inactive_user_workflow']['second_notice_ts'] = [
    'title' => t('Second notice'),
    'help' => t('The date and time when second notice was sent.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  $data['inactive_user_workflow']['blocked_ts'] = [
    'title' => t('Blocked'),
    'help' => t('The date and time when the user was blocked.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  $data['inactive_user_workflow']['logged_in_ts'] = [
    'title' => t('Logged in'),
    'help' => t('The date and time when the user logged in.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  $data['inactive_user_workflow']['current_status'] = [
    'title' => t('Current status'),
    'help' => t('Current status in the workflow.'),
    'field' => [
      'id' => 'machine_name',
      'options callback' => 'Drupal\inactive_user_workflow\InactiveUser::getStatusOptions',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['inactive_user_workflow']['archived'] = [
    'title' => t('Archived'),
    'help' => t('Weather this workflow is active or archived.'),
    'field' => [
      'id' => 'boolean',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'boolean',
      'label' => t('Archived'),
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  return $data;
}
