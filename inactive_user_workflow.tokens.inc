<?php

/**
 * @file
 * Builds placeholder replacement tokens for this module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function inactive_user_workflow_token_info() {
  $site['inactive-user-workflow-max-days-to-start'] = [
    'name' => t('Amount of days that the user is allowed not to log in to the site'),
    'description' => t('Inactive users flow: Amount of days that the user is allowed not to log in to the site.'),
  ];

  $site['inactive-user-workflow-days-to-follow-up-1'] = [
    'name' => t('Days to wait after initial email to send a reminder'),
    'description' => t('Inactive users flow: Days to wait after initial email to send a reminder.'),
  ];

  $site['inactive-user-workflow-days-to-follow-up-2'] = [
    'name' => t('Days to wait after follow-up email to block account'),
    'description' => t('Inactive users flow: Days to wait after follow-up email to block account.'),
  ];

  $site['inactive-user-workflow-contact-email'] = [
    'name' => t('Contact email'),
    'description' => t('Inactive users flow: Contact email for replies and assistance.'),
  ];

  return [
    'types' => [],
    'tokens' => [
      'site' => $site,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function inactive_user_workflow_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'site') {
    foreach ($tokens as $name => $original) {
      $config = \Drupal::config('inactive_user_workflow.settings');

      switch ($name) {
        case 'inactive-user-workflow-max-days-to-start':
          $replacements[$original] = $config->get('max_days_to_start', '');
          break;

        case 'inactive-user-workflow-days-to-follow-up-1':
          $replacements[$original] = $config->get('days_to_follow_up_1', '');
          break;

        case 'inactive-user-workflow-days-to-follow-up-2':
          $replacements[$original] = $config->get('days_to_follow_up_2', '');
          break;

        case 'inactive-user-workflow-contact-email':
          $replacements[$original] = $config->get('contact_email', '');
          break;
      }
    }
  }

  return $replacements;
}
