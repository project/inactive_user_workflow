<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow;

/**
 * Provides an interface to represent an inactive user's workflow.
 */
interface InactiveUserInterface {

  const STATUS_NOT_STARTED = 0;
  const STATUS_FIRST_NOTICE_SENT = 1;
  const STATUS_SECOND_NOTICE_SENT = 2;
  const STATUS_ACCOUNT_BLOCKED = 3;
  const STATUS_ABORTED_BECAUSE_LOGGED_IN = 4;
  const STATUS_ABORTED_BECAUSE_RECOVERED = 5;

  /**
   * Get this workflow ID.
   *
   * @return int|null
   *   The workflow ID, or NULL if this is a new workflow.
   */
  public function getId(): ?int;

  /**
   * Get the associated user ID.
   *
   * @return int|null
   *   The user ID, or NULL if this is an empty workflow.
   */
  public function getUid(): ?int;

  /**
   * Set the blocked timestamp when the first notice was sent.
   *
   * @param int|null $first_notice_ts
   *   The timestamp when the first notice was sent.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setFirstNoticeTimestamp(?int $first_notice_ts = NULL): InactiveUserInterface;

  /**
   * Get the timestamp when the first notice was sent.
   *
   * @return int|null
   *   The timestamp when the first notice was sent.
   */
  public function getFirstNoticeTimestamp(): ?int;

  /**
   * Set the blocked timestamp when the second notice was sent.
   *
   * @param int|null $second_notice_ts
   *   The timestamp when the second notice was sent.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setSecondNoticeTimestamp(?int $second_notice_ts = NULL): InactiveUserInterface;

  /**
   * Get the timestamp when the second notice was sent.
   *
   * @return int|null
   *   The timestamp when the second notice was sent.
   */
  public function getSecondNoticeTimestamp(): ?int;

  /**
   * Set the blocked timestamp.
   *
   * @param int|null $blocked_ts
   *   The timestamp when the account was blocked.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setBlockedTimestamp(?int $blocked_ts = NULL): InactiveUserInterface;

  /**
   * Get the timestamp when the account was blocked.
   *
   * @return int|null
   *   The timestamp when the account was blocked.
   */
  public function getBlockedTimestamp(): ?int;

  /**
   * Set the timestamp when the account logged in and this workflow stopped.
   *
   * @param int|null $logged_in_ts
   *   The timestamp when the account logged in.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setLoggedInTimestamp(?int $logged_in_ts = NULL): InactiveUserInterface;

  /**
   * Get the timestamp when the account logged in.
   *
   * @return int|null
   *   The timestamp when the account logged in.
   */
  public function getLoggedInTimestamp(): ?int;

  /**
   * Get the current status in the workflow.
   *
   * @return int
   *   The current status in the workflow.
   */
  public function getCurrentStatus(): int;

  /**
   * Set the current status in the workflow.
   *
   * @param int $current_status
   *   The current status in the workflow.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setCurrentStatus(int $current_status): InactiveUserInterface;

  /**
   * Set the archive property.
   *
   * @param bool $archived
   *   If the workflow is active or archived.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public function setArchived(bool $archived): InactiveUserInterface;

  /**
   * If the workflow is active or archived.
   *
   * @return bool
   *   If the workflow is active or archived.
   */
  public function isArchived(): bool;

  /**
   * If the workflow is new (not saved).
   *
   * @return bool
   *   If the workflow is new (not saved).
   */
  public function isNew(): bool;

  /**
   * Get the latest active workflow for a user.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  public static function getActiveWorkflowForUser(int $uid): InactiveUserInterface;

  /**
   * Save the workflow.
   */
  public function save(): void;

  /**
   * Get the status options.
   *
   * @return array
   *   The status options.
   */
  public static function getStatusOptions(): array;

}
