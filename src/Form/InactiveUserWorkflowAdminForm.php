<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a setting UI for this module.
 */
class InactiveUserWorkflowAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'inactive_user_workflow.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inactive_user_workflow_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('inactive_user_workflow.settings');

    $form['description'] = [
      '#type' => 'item',
      '#description' => $this->t('If enabled, users who have not logged in for a certain amount of time will be prompted to log in. If they do not log in after a certain amount of time, their account will be blocked.<br/>If disabled, the workflow will not run and no notifications will be sent.'),
    ];

    $form['is_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the inactive user workflow'),
      '#default_value' => $config->get('is_enabled'),
    ];

    $form['login_or_access'] = [
      '#type' => 'radios',
      '#title' => $this->t('Check the "last login" or "last access" field to determine if user is inactive'),
      '#default_value' => $config->get('login_or_access'),
      '#required' => TRUE,
      '#options' => [
        'last_login' => $this->t('Last login'),
        'last_access' => $this->t('Last access'),
      ],
    ];

    $form['max_days_to_start'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of days that the user is allowed not to log in to the site'),
      '#required' => TRUE,
      '#default_value' => $config->get('max_days_to_start'),
    ];

    $form['days_to_follow_up_1'] = [
      '#type' => 'number',
      '#title' => $this->t('Days to wait after initial email to send a reminder'),
      '#required' => TRUE,
      '#default_value' => $config->get('days_to_follow_up_1'),
    ];

    $form['days_to_follow_up_2'] = [
      '#type' => 'number',
      '#title' => $this->t('Days to wait after follow-up email to block account'),
      '#required' => TRUE,
      '#default_value' => $config->get('days_to_follow_up_2'),
    ];

    $form['template_subject_first_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject of mail to send to prompt user to log in the first time'),
      '#default_value' => $config->get('template_subject_first_mail'),
    ];

    $form['template_body_first_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of mail to send to prompt user to log in the first time'),
      '#default_value' => $config->get('template_body_first_mail'),
    ];

    $form['template_subject_second_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject of mail to follow up with a reminder'),
      '#default_value' => $config->get('template_subject_second_mail'),
    ];

    $form['template_body_second_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of mail to follow up with a reminder'),
      '#default_value' => $config->get('template_body_second_mail'),
    ];

    $form['template_subject_third_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject of mail to inform user of account block'),
      '#default_value' => $config->get('template_subject_third_mail'),
    ];

    $form['template_body_third_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of mail to inform user of account block'),
      '#default_value' => $config->get('template_body_third_mail'),
    ];

    $form['template_subject_third_mail_to_admin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject of mail to inform admin of account block'),
      '#default_value' => $config->get('template_subject_third_mail_to_admin'),
    ];

    $form['template_body_third_mail_to_admin'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of mail to inform admin of account block'),
      '#default_value' => $config->get('template_body_third_mail_to_admin'),
    ];

    $form['contact_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Contact email for replies and assistance'),
      '#description' => $this->t('Will also be used as reply-to.'),
      '#default_value' => $config->get('contact_email'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('inactive_user_workflow.settings');
    $config
      ->set('is_enabled', (bool) $values['is_enabled'])
      ->set('login_or_access', $values['login_or_access'])
      ->set('max_days_to_start', $values['max_days_to_start'])
      ->set('days_to_follow_up_1', $values['days_to_follow_up_1'])
      ->set('days_to_follow_up_2', $values['days_to_follow_up_2'])
      ->set('template_subject_first_mail', $values['template_subject_first_mail'])
      ->set('template_body_first_mail', $values['template_body_first_mail'])
      ->set('template_subject_second_mail', $values['template_subject_second_mail'])
      ->set('template_body_second_mail', $values['template_body_second_mail'])
      ->set('template_subject_third_mail', $values['template_subject_third_mail'])
      ->set('template_body_third_mail', $values['template_body_third_mail'])
      ->set('template_subject_third_mail_to_admin', $values['template_subject_third_mail_to_admin'])
      ->set('template_body_third_mail_to_admin', $values['template_body_third_mail_to_admin'])
      ->set('contact_email', $values['contact_email']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
