<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow;

use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Psr\Log\LoggerInterface;

/**
 * Represents an inactive user's workflow.
 */
class InactiveUser implements InactiveUserInterface {

  /**
   * The status options for the workflow.
   *
   * @var array
   */
  private static array $statusOptions = [];

  /**
   * The workflow id.
   *
   * @var int
   */
  protected int $id;

  /**
   * The user id.
   *
   * @var int
   */
  protected int $uid;

  /**
   * The timestamp the first notice was sent.
   *
   * @var int|null
   */
  protected ?int $firstNoticeTimestamp = NULL;

  /**
   * The timestamp the second notice was sent.
   *
   * @var int|null
   */
  protected ?int $secondNoticeTimestamp = NULL;

  /**
   * The timestamp the account was blocked.
   *
   * @var int|null
   */
  protected ?int $blockedTimestamp = NULL;

  /**
   * The timestamp of when account logged in (to abort workflow).
   *
   * @var int|null
   */
  protected ?int $loggedInTimestamp = NULL;

  /**
   * The current status in the workflow.
   *
   * @var int
   */
  protected int $currentStatus = self::STATUS_NOT_STARTED;

  /**
   * If the workflow is active or archived.
   *
   * @var bool
   */
  protected bool $archived = FALSE;

  /**
   * InactiveUser constructor.
   *
   * @param int $uid
   *   The user ID.
   */
  public function __construct(int $uid) {
    $this->uid = $uid;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): ?int {
    return $this->id;
  }

  /**
   * Set the workflow id.
   *
   * @param int $id
   *   The workflow id.
   *
   * @return \Drupal\inactive_user_workflow\InactiveUserInterface
   *   The called workflow entity.
   */
  protected function setId(int $id): InactiveUserInterface {
    $this->id = $id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUid(): ?int {
    return $this->uid;
  }

  /**
   * {@inheritdoc}
   */
  public function setFirstNoticeTimestamp(?int $first_notice_ts = NULL): InactiveUserInterface {
    $this->firstNoticeTimestamp = $first_notice_ts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstNoticeTimestamp(): ?int {
    return $this->firstNoticeTimestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function setSecondNoticeTimestamp(?int $second_notice_ts = NULL): InactiveUserInterface {
    $this->secondNoticeTimestamp = $second_notice_ts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSecondNoticeTimestamp(): ?int {
    return $this->secondNoticeTimestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function setBlockedTimestamp(?int $blocked_ts = NULL): InactiveUserInterface {
    $this->blockedTimestamp = $blocked_ts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockedTimestamp(): ?int {
    return $this->blockedTimestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoggedInTimestamp(?int $logged_in_ts = NULL): InactiveUserInterface {
    $this->loggedInTimestamp = $logged_in_ts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLoggedInTimestamp(): ?int {
    return $this->loggedInTimestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentStatus(): int {
    return $this->currentStatus;
  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentStatus(int $current_status): InactiveUserInterface {
    $this->currentStatus = $current_status;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setArchived(bool $archived): InactiveUserInterface {
    $this->archived = $archived;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isArchived(): bool {
    return $this->archived;
  }

  /**
   * {@inheritdoc}
   */
  public function isNew(): bool {
    return empty($this->id);
  }

  /**
   * {@inheritdoc}
   */
  public static function getActiveWorkflowForUser(int $uid): InactiveUserInterface {
    $result = \Drupal::database()->select('inactive_user_workflow', 'i')
      ->fields('i')
      ->condition('i.uid', $uid)
      ->condition('i.archived', 0)
      ->orderBy('i.id', 'DESC')
      ->range(0, 1)
      ->execute()
      ->fetchObject();

    $workflow = new static($uid);

    if (!empty($result)) {
      $workflow
        ->setId((int) $result->id)
        ->setFirstNoticeTimestamp((int) $result->first_notice_ts ?? NULL)
        ->setSecondNoticeTimestamp((int) $result->second_notice_ts ?? NULL)
        ->setBlockedTimestamp((int) $result->blocked_ts ?? NULL)
        ->setLoggedInTimestamp((int) $result->logged_in_ts ?? NULL)
        ->setCurrentStatus((int) $result->current_status)
        ->setArchived((bool) $result->archived);
    }

    return $workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function save(): void {
    try {
      if ($this->isNew()) {
        $id = $this->database()
          ->insert('inactive_user_workflow')
          ->fields([
            'uid' => $this->getUid(),
            'first_notice_ts' => $this->getFirstNoticeTimestamp(),
            'current_status' => $this->getCurrentStatus(),
            'archived' => 0,
          ])
          ->execute();
        $this->setId((int) $id);
      }
      else {
        $this->database()
          ->update('inactive_user_workflow')
          ->fields([
            'id' => $this->getId(),
            'uid' => $this->getUid(),
            'first_notice_ts' => $this->getFirstNoticeTimestamp() ?: NULL,
            'second_notice_ts' => $this->getSecondNoticeTimestamp() ?: NULL,
            'blocked_ts' => $this->getBlockedTimestamp() ?: NULL,
            'logged_in_ts' => $this->getLoggedInTimestamp() ?: NULL,
            'current_status' => $this->getCurrentStatus(),
            'archived' => (int) $this->isArchived(),
          ])
          ->condition('id', $this->getId())
          ->condition('uid', $this->getUid())
          ->execute();
      }
    }
    catch (\Exception $e) {
      $this->logger()->error('Unable to save inactive user workflow update for user %uid: %error', [
        '%uid' => $this->getUid(),
        '%error' => $e->getMessage(),
      ]);
    }
  }

  /**
   * Gets the database service.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  protected function database(): Connection {
    return \Drupal::database();
  }

  /**
   * Gets the logger service for this module.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  protected function logger(): LoggerInterface {
    return \Drupal::service('logger.channel.inactive_user_workflow');
  }

  /**
   * {@inheritdoc}
   */
  public static function getStatusOptions(): array {
    if (empty(static::$statusOptions)) {
      $options = [
        static::STATUS_NOT_STARTED => new TranslatableMarkup('Not started'),
        static::STATUS_FIRST_NOTICE_SENT => new TranslatableMarkup('First notice sent'),
        static::STATUS_SECOND_NOTICE_SENT => new TranslatableMarkup('Second notice sent'),
        static::STATUS_ACCOUNT_BLOCKED => new TranslatableMarkup('Account blocked'),
        static::STATUS_ABORTED_BECAUSE_LOGGED_IN => new TranslatableMarkup('Aborted (user logged in)'),
      ];

      // Give other modules a chance to alter the list of status options.
      \Drupal::moduleHandler()->alter('inactive_user_workflow_status_options', $options);

      static::$statusOptions = $options;
    }

    return static::$statusOptions;
  }

}
