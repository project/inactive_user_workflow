<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow;

/**
 * Interface for the service that manages inactive user workflows.
 */
interface InactiveUsersWorkflowServiceInterface {

  /**
   * Run the inactive users workflow job.
   */
  public function job(): void;

}
