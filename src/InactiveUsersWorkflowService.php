<?php

declare(strict_types = 1);

namespace Drupal\inactive_user_workflow;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;

/**
 * Service that manages inactive user workflows.
 */
class InactiveUsersWorkflowService implements InactiveUsersWorkflowServiceInterface {

  /**
   * The configuration for this service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected readonly EntityStorageInterface $userStorage;

  /**
   * The time this job started executing.
   *
   * @var int
   */
  protected int $timestamp;

  /**
   * Constructs an InactiveUsersWorkflowService object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly Connection $connection,
    private readonly MailManagerInterface $pluginManagerMail,
    private readonly LoggerInterface $logger,
  ) {
    $this->config = $configFactory->get('inactive_user_workflow.settings');
    $this->userStorage = $this->entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function job(): void {
    // Only run if enabled in module config.
    if (!$this->config->get('is_enabled')) {
      return;
    }

    $this->timestamp = time();
    $inactiveUsersIds = $this->getInactiveUsers();
    $this->jobBlockUser($inactiveUsersIds);
    $this->jobSecondNotice($inactiveUsersIds);
    $this->jobFirstNotice($inactiveUsersIds);
  }

  /**
   * Get a list of user Ids that haven't logged in since the cutoff time.
   *
   * @return array
   *   The user IDs.
   */
  protected function getInactiveUsers(): array {
    $cutoffFirstNotice = $this->timestamp - ($this->config->get('max_days_to_start') * 24 * 60 * 60);
    $lastActivityField = ($this->config->get('login_or_access') == 'last_login') ? 'login' : 'access';
    $query = $this->userStorage
      ->getQuery()
      ->condition('status', 1)
      ->condition($lastActivityField, $cutoffFirstNotice, '<=')
      ->accessCheck(FALSE);

    return $query->execute();
  }

  /**
   * Send out first notice to users.
   *
   * @param array $inactiveUsersIds
   *   Users that are inactive, will only send first notice if applicable.
   */
  protected function jobFirstNotice(array $inactiveUsersIds): void {
    foreach ($inactiveUsersIds as $accountId) {
      $workflow = InactiveUser::getActiveWorkflowForUser((int) $accountId);

      // If workflow isn't new, it means it already sent other notification.
      if ($workflow->isNew()) {
        /** @var \Drupal\user\UserInterface $account */
        $account = $this->userStorage->load($accountId);
        $this->sendNotification(
          $this->config->get('template_subject_first_mail'),
          $this->config->get('template_body_first_mail'),
          $account->getEmail(),
          $account
        );
        $workflow
          ->setCurrentStatus(InactiveUserInterface::STATUS_FIRST_NOTICE_SENT)
          ->setFirstNoticeTimestamp($this->timestamp)
          ->save();
      }
    }
  }

  /**
   * Send out second notice to users.
   *
   * @param array $inactiveUsersIds
   *   Users that are inactive, will only send second notice if applicable.
   */
  protected function jobSecondNotice(array $inactiveUsersIds): void {
    $workflowsUids = $this->connection->select('inactive_user_workflow', 'iuw')
      ->fields('iuw', ['uid'])
      ->condition('archived', 0)
      ->condition('current_status', InactiveUserInterface::STATUS_FIRST_NOTICE_SENT)
      ->isNull('second_notice_ts')
      ->condition('uid', $inactiveUsersIds, 'IN')
      ->where('(first_notice_ts + :cutoff_since) <= :current', [
        ':cutoff_since' => $this->config->get('days_to_follow_up_1') * 24 * 60 * 60,
        ':current' => $this->timestamp,
      ])
      ->execute()
      ->fetchAllAssoc('uid');

    foreach ($workflowsUids as $workflowsUid) {
      $workflow = InactiveUser::getActiveWorkflowForUser((int) $workflowsUid->uid);
      if (!$workflow->isNew()) {
        /** @var \Drupal\user\UserInterface $account */
        $account = $this->userStorage->load($workflow->getUid());
        $this->sendNotification(
          $this->config->get('template_subject_second_mail'),
          $this->config->get('template_body_second_mail'),
          $account->getEmail(),
          $account
        );
        $workflow
          ->setCurrentStatus(InactiveUserInterface::STATUS_SECOND_NOTICE_SENT)
          ->setSecondNoticeTimestamp($this->timestamp)
          ->save();
      }
    }
  }

  /**
   * Block users.
   *
   * @param array $inactiveUsersIds
   *   Users that are inactive, will only block if applicable.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function jobBlockUser(array $inactiveUsersIds): void {
    $workflowsUids = $this->connection->select('inactive_user_workflow', 'iuw')
      ->fields('iuw', ['uid'])
      ->condition('archived', 0)
      ->condition('current_status', InactiveUserInterface::STATUS_SECOND_NOTICE_SENT)
      ->isNull('blocked_ts')
      ->condition('uid', $inactiveUsersIds, 'IN')
      ->where('(second_notice_ts + :cutoff_since) <= :current', [
        ':cutoff_since' => $this->config->get('days_to_follow_up_2') * 24 * 60 * 60,
        ':current' => $this->timestamp,
      ])
      ->execute()
      ->fetchAllAssoc('uid');

    foreach ($workflowsUids as $workflowsUid) {
      $workflow = InactiveUser::getActiveWorkflowForUser((int) $workflowsUid->uid);
      if (!$workflow->isNew()) {
        /** @var \Drupal\user\UserInterface $account */
        $account = $this->userStorage->load($workflow->getUid());
        $account
          ->block()
          ->save();
        $this->logger->notice('Blocked user %mail.', [
          '%mail' => $account->getEmail(),
        ]);

        // Send notifications to user and admin.
        $this->sendNotification(
          $this->config->get('template_subject_third_mail'),
          $this->config->get('template_body_third_mail'),
          $account->getEmail(),
          $account
        );
        $this->sendNotification(
          $this->config->get('template_subject_third_mail_to_admin'),
          $this->config->get('template_body_third_mail_to_admin'),
          $this->config->get('contact_email'),
          $account
        );
        $workflow
          ->setCurrentStatus(InactiveUserInterface::STATUS_ACCOUNT_BLOCKED)
          ->setBlockedTimestamp($this->timestamp)
          ->save();
      }
    }
  }

  /**
   * Send the email notification.
   *
   * @param string $subject
   *   The subject string.
   * @param string $body
   *   The body of the mail.
   * @param string $to
   *   The email.
   * @param \Drupal\user\UserInterface $account
   *   The user.
   *
   * @see inactive_user_workflow_mail()
   */
  protected function sendNotification(string $subject, string $body, string $to, UserInterface $account) {
    $params['subject'] = $subject;
    $params['body'] = $body;
    $params['to'] = $to;
    $params['reply-to'] = $this->config->get('contact_email');
    $params['account'] = $account;
    $params['language'] = $account->language()->getId();

    $this->pluginManagerMail->mail('inactive_user_workflow', 'first_notice', $to, $account->language()->getId(), $params);
  }

}
